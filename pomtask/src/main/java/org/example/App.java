package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;





/**
 * Hello world!
 *
 */
public class App 
{
    WebDriver webDriver;

    String webPage="https://pastebin.com/";

    By NewPasteFieldText= By.xpath("/html/body/div[1]/div[2]/div[1]/div[2]/div/form/div[3]/textarea");

    By PasteExpirationOptionMenu=By.xpath("/html/body/div[1]/div[2]/div[1]/div[2]/div/form/div[5]/div[1]/div[4]/div/span/span[1]/span/span[1]");

    By SelectedOption=By.xpath("/html/body/span[2]/span/span[2]/ul/li[3]");

    By NewPasteTitleName=By.xpath("/html/body/div[1]/div[2]/div[1]/div[2]/div/form/div[5]/div[1]/div[9]/div/input");

    By CreateNewPasteButton=By.xpath("/html/body/div[1]/div[2]/div[1]/div[2]/div/form/div[5]/div[1]/div[10]/button");



    public App(WebDriver webDriver){
        this.webDriver=webDriver;
    }

    public void openPage(){
        webDriver.get(webPage);


    }

    public void writeNewPasteText(){

        webDriver.findElement(NewPasteFieldText).sendKeys("Hello from WebDriver");

    }
    public void clickPasteExpirationOptionMenu(){
        webDriver.findElement(PasteExpirationOptionMenu).click();
    }
    public void clickSelectedOption(){
        webDriver.findElement(SelectedOption).click();
    }
    public void writeNewPasteTitleName(){
        webDriver.findElement(NewPasteTitleName).sendKeys("hello web");

    }
    public void clickCreateNewPasteButton(){
        webDriver.findElement(CreateNewPasteButton).click();
    }

}
